(function() {
    while (true) {
        var loadMoreButtons = document.getElementsByClassName('row load-more')[0].getElementsByTagName('button');
        if (loadMoreButtons.length) {
            var loadMoreButton = loadMoreButtons[0];
            loadMoreButton.click();
        } else {
            break;
        }
    }

    var transactions = document.getElementsByClassName('transaction-row hidden-xs');
    var csvContent = "data:text/csv;charset=utf-8,";
    for (transaction of transactions) {
        var date = transaction.getElementsByClassName('date')[0].innerText;
        var description = '"' + transaction.getElementsByClassName('description')[0].innerText + '"';
        var amount = transaction.getElementsByClassName('amount')[0].innerText.replace(/[R\s]/g, '');
        var balance = transaction.getElementsByClassName('balance')[0].innerText.replace(/[R\s]/g, '');
        csvContent += [date, description, amount, balance].join(',') + '\n';
    }
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement('a');
    link.setAttribute('href', encodedUri);
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    var accountLabelContainer = document.getElementsByClassName('account-label');
    var accountLabel;
    if (accountLabelContainer.length) {
        accountLabel = accountLabelContainer[0].nextSibling.nextSibling.textContent;  // everyday account
    } else {
        accountLabel = document.getElementsByClassName('container goal-save-detail')[0].getElementsByTagName('h1')[0].innerText;  // goalsave
    }
    link.setAttribute('download', yyyy +'-' + mm + '-' + dd + ' ' + accountLabel + '.csv');
    document.body.appendChild(link);
    link.click();
})();
